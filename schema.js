const axios = require('axios');

// Import GraphQL types
const {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLString,
  GraphQLBoolean,
  GraphQLList,
  GraphQLSchema,
  GraphQLFloat
} = require('graphql');

// Restaurant Type
const RestaurantType = new GraphQLObjectType({
  name: 'Restaurant',
  fields: () => ({
    id: { type: GraphQLString },
    alias: { type: GraphQLString },
    name: { type: GraphQLString },
    is_closed: { type: GraphQLBoolean },
    url: { type: GraphQLString },
    review_count: { type: GraphQLInt },
    rating: { type: GraphQLFloat },
    display_phone: { type: GraphQLString },
    location: { type: LocationType }
  })
});

// Location Type
const LocationType = new GraphQLObjectType({
  name: 'Location',
  fields: () => ({
    address1: { type: GraphQLString },
    city: { type: GraphQLString },
    zip_code: { type: GraphQLString },
    state: { type: GraphQLString }
  })
});

// Root Query
const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    businesses: {
      type: new GraphQLList(RestaurantType),
      args: {
        location: { type: GraphQLString }
      },
      resolve(parent, args) {
        // GET all restaurants by location
        return axios
          .get(
            'https://api.yelp.com/v3/businesses/search?location='+args.location,
            {
              headers: {
                Authorization:
                  'Bearer iAcCDHxc0Vw6DWzLclhkBSz_3Bxpj5pUpQNGJsrI-B8mGFxkM9zbFqTLezQXX4o9cjbMJF753P3Kn91C8Dhm4FhrRgGbKoBMHDgK99x3gDxxGiJ-CwBdYszcvmomXnYx'
              }
            }
          )
          .then(res => res.data.businesses);
      }
    }
  }
});

module.exports = new GraphQLSchema({
  query: RootQuery
});
