# Restaurant Reservation

## Setup and Usage Without Docker
#### To setup without Docker:
In project root folder: 

Change origin in corsOptions in server.js to origin: 'http://localhost:3000'

npm install

cd client

npm install

#### To run server without Docker:
In project root folder: 

npm run server

#### To run client without Docker:
In project root folder: 

npm run client

#### To run client and server without Docker:
In project root folder: 

npm run dev

#### Usage without Docker:
Navigate to http://localhost:3000

Enter a location and click Submit to search restaurants by location

Click Schedule Reservation to schedule a reservation at a restaurant

Once a reservation has been scheduled you can modify or cancel it

## Setup and Usage with Docker
#### To build server with Docker:
In project root folder: 

Change origin in corsOptions in server.js to origin: 'http://localhost:3001'

sudo docker build -t username/node-web-app .

#### To run server with Docker:
In project root folder: 

sudo docker run -p 5000:5000 username/node-web-app

#### To build client with Docker:

cd client

sudo docker build -t username:react-web-app .

#### To run client with Docker:

In client folder: 

sudo docker run -v ${PWD}:/app -v /app/node_modules -p 3001:3000 --rm username:react-web-app

#### Usage with Docker:

Navigate to http://localhost:3001

Enter a location and click Submit to search restaurants by location

Click Schedule Reservation to schedule a reservation at a restaurant

Once a reservation has been scheduled you can modify or cancel it
