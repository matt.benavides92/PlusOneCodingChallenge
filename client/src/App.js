import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Restaurants from './components/Restaurants';
import './App.css';

// Setup ApolloClient
const client = new ApolloClient({
  uri: 'http://localhost:5000/graphql'
});

class App extends Component {
  render() {
    return (
      // Enable Apollo client
      <ApolloProvider client={client}>
        <Router>
          <div className="container">
            <Route exact path="/" component={Restaurants} />
          </div>
        </Router>
      </ApolloProvider>
    );
  }
}

export default App;
