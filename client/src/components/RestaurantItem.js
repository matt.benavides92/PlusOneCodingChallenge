import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import RestaurantCard from './RestaurantCard';
import M from 'materialize-css';

// Receives restaurant details from GraphQL query
export default function RestaurantItem({
  // Grab restaurant details
  restaurant: { id, alias, name, is_closed, url, review_count, rating, display_phone, location }
}) {
  // Render RestaurantCard and pass in restaurant details as props
  return (
    <RestaurantCard
      id={id}
      alias={alias}
      name={name}
      is_closed={is_closed}
      url={url}
      review_count={review_count}
      rating={rating}
      display_phone={display_phone}
      location={location}
    />
  );
}
