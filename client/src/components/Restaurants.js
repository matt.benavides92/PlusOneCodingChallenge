import React, { Component, Fragment } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import RestaurantItem from './RestaurantItem';
import RestaurantsQuery from './RestaurantsQuery';

// Query to grab restaurants
const RESTAURANTS_QUERY = gql`
  {
    businesses {
      id
      alias
      name
      is_closed
      url
      rating
      location {
        address1
        city
        zip_code
        state
      }
    }
  }
`;

export class Restaurants extends Component {
  constructor(props) {
    super(props);
    // Store location entered in form as state
    this.state = {
      location: null
    };
  }

  // Stores location as state when submit button clicked
  handleSubmitButton = () => {
    this.setState({ location: document.getElementById('location').value });
  };

  render() {
    let restaurantsQuery;
    // check if a location was entered
    if (this.state.location) {
      // Pass in location to query component
      restaurantsQuery = <RestaurantsQuery location={this.state.location} />;
    }
    return (
      // Display headers and location form field
      <Fragment>
        <h1 className="display-4 my-3">Restaurant Reservation</h1>
        <h4>Search restaurants in a specific location</h4>
        <div className="row">
          <form className="col s12">
            <div className="row">
              {/* Location input field */}
              <div className="input-field col s9">
                <input
                  placeholder="Location"
                  id="location"
                  name="location"
                  type="text"
                />
                <label for="location">Location</label>
              </div>
              {/* Submit button */}
              <div className="input-field col s3">
                <a
                  className="btn"
                  onClick={this.handleSubmitButton}
                >
                  Submit
                </a>
              </div>
            </div>
          </form>
        </div>
        {/* Display restaurant query results */}
        {restaurantsQuery}
      </Fragment>
    );
  }
}

export default Restaurants;
