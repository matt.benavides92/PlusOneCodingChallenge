import React, { Component, Fragment } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import RestaurantItem from './RestaurantItem';

// Query to get restaurants by location
const BUSINESSES_QUERY = gql`
  query BusinessesQuery($location: String!) {
    businesses(location: $location) {
      id
      alias
      name
      is_closed
      url
      review_count
      rating
      display_phone
      location {
        address1
        city
        zip_code
        state
      }
    }
  }
`;

export class RestaurantsQuery extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        {/* Query restaurants */}
        <Query query={BUSINESSES_QUERY} variables={this.props}>
          {({ loading, error, data }) => {
            // Check if loading or error
            if (loading) return 'Loading...';
            if (error) console.log(error);

            // Returns RestaurantItem with data filled from query
            return (
              <Fragment>
                {/* Loop through query results and render each in RestaurantItem */}
                {data.businesses.map(restaurant => (
                  <RestaurantItem key={restaurant.id} restaurant={restaurant} />
                ))}
              </Fragment>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default RestaurantsQuery;
