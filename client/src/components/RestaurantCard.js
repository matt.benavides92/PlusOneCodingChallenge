import React, { Component } from 'react';
import M from 'materialize-css';

export class RestaurantCard extends Component {
  constructor(props) {
    super(props);
  }

  // Initialize Materialize CSS Javascript elements
  componentDidMount() {
    var modalElems = document.querySelectorAll('.modal');
    var modalInstances = M.Modal.init(modalElems);
    var datePickerElems = document.querySelectorAll('.datepicker');
    var datePickerInstances = M.Datepicker.init(datePickerElems);
    var timePickerElems = document.querySelectorAll('.timepicker');
    var timePickerInstances = M.Timepicker.init(timePickerElems);
  }

  // Handle modal form submission
  handleModalSubmit = businessId => {
    // Get modal form field values
    const customerName = document.getElementById('nameId=' + businessId).value;
    const customerPhoneNumber = document.getElementById(
      'phoneNumberId=' + businessId
    ).value;
    const reservationDate = document.getElementById('dateId=' + businessId)
      .value;
    const reservationTime = document.getElementById('timeId=' + businessId)
      .value;

    // Ensure all form fields are filled out
    if (customerName === '') {
      alert('Name is required');
      return;
    }

    if (customerPhoneNumber === '') {
      alert('Phone number is required');
      return;
    }
    if (reservationDate === '') {
      alert('Date is required');
      return;
    }
    if (reservationTime === '') {
      alert('Time is required');
      return;
    }

    // Create JSON object to store reservation details
    var businessReservation = {
      id: businessId,
      name: customerName,
      phoneNumber: customerPhoneNumber,
      date: reservationDate,
      time: reservationTime
    };

    // Store JSON object as string in localStorage with business ID as key
    localStorage.setItem(businessId, JSON.stringify(businessReservation));

    alert('Your reservation has been scheduled');
    this.forceUpdate(); // update DOM
  };

  // Prefills reservation details in modal form if a reservation exists
  displayCurrentReservation = id => {
    // Get reservation object from localStorage and parse it into an object
    var reservationObject = JSON.parse(localStorage.getItem(id));

    if (reservationObject !== null) {
      // Fill form fields with reservation details
      document.getElementById('nameId=' + id).value = reservationObject.name;
      document.getElementById('phoneNumberId=' + id).value =
        reservationObject.phoneNumber;
      document.getElementById('dateId=' + id).value = reservationObject.date;
      document.getElementById('timeId=' + id).value = reservationObject.time;
    }
  };

  // Cancels a reservation
  cancelReservation = id => {
    // Remove reservation details from localStorage by business ID
    localStorage.removeItem(id);
    alert('Your reservation has been cancelled');
    this.forceUpdate(); // update DOM
  };

  // Renders either Modify or Schedule button
  renderReservationButton = id => {
    // Check if a reservation exists for ID in localStorage
    if (localStorage.getItem(id)) {
      // Reservation exists, use display button
      return (
        <a
          className="btn modal-trigger"
          href={this.getModalId(id, true)}
          onClick={e => this.displayCurrentReservation(id)}
          style={{ marginBottom: '3%', width: '100%' }}
        >
          Modify Reservation
        </a>
      );
    } else {
      // No reservation, display Schedule button
      return (
        <a
          className="btn modal-trigger"
          href={this.getModalId(id, true)}
          onClick={e => this.displayCurrentReservation(id)}
          style={{ width: '100%'}}
        >
          Schedule Reservation
        </a>
      );
    }
  };

  // Renders cancel button if reservation exists
  renderCancelButton = id => {
    // Check if reservation exists in localStorage by business ID
    if (localStorage.getItem(id)) {
      // Reservation exists, render cancel button
      return (
        <a
          className="btn modal-trigger"
          onClick={e => this.cancelReservation(id)}
          style={{ width: '100%' }}
        >
          Cancel Reservation
        </a>
      );
    }
  };

  // Returns modal ID for modal div id
  getModalId = (id, needHref) => {
    if (needHref) {
      return '#modalId=' + id;
    } else {
      return 'modalId=' + id;
    }
  };

  // Returns name ID for name input form field
  getNameId = id => {
    return 'nameId=' + id;
  };

  // Returns phone number ID for phone number input form field
  getPhoneNumberId = id => {
    return 'phoneNumberId=' + id;
  };

  // Returns date ID for date input form field
  getDateId = id => {
    return 'dateId=' + id;
  };

  // Returns time ID for time input form field
  getTimeId = id => {
    return 'timeId=' + id;
  };

  render() {
    return (
      <div className="row">
        <div className="col s12 m6">
          <div className="card blue-grey darken-1">
            <div className="card-content white-text">
              {/* Display restaurant details  */}
              <span className="card-title">{this.props.name}</span>
              <p>Rating: {this.props.rating}</p>
              <p>Review Count: {this.props.review_count}</p>
              <p>Phone Number: {this.props.display_phone}</p>
              <p>
                Address: {this.props.location.address1},{' '}
                {this.props.location.city}, {this.props.location.state},{' '}
                {this.props.location.zip_code}
              </p>
            </div>
            <div className="card-action">
              {/* Render appropriate reservation button and cancel button if appropriate */}
              {this.renderReservationButton(this.props.id)}
              {this.renderCancelButton(this.props.id)}
              {/* Modal form to get reservation details */}
              <div id={this.getModalId(this.props.id, false)} class="modal">
                <div class="modal-content">
                  <h4>Schedule a reservation</h4>
                  <div className="row">
                    <form className="col s12">
                      <div className="row">
                        <div className="input-field col s6">
                          {/* Name input */}
                          <input
                            placeholder="Name"
                            id={this.getNameId(this.props.id)}
                            name="name"
                            type="text"
                          />
                        </div>
                        <div className="input-field col s6">
                          {/* Phone number input */}
                          <input
                            placeholder="Phone Number"
                            id={this.getPhoneNumberId(this.props.id)}
                            name="phonenumber"
                            type="text"
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="input-field col s6">
                          {/* Date input */}
                          <input
                            placeholder="Date"
                            id={this.getDateId(this.props.id)}
                            name="date"
                            type="text"
                            className="datepicker"
                          />
                        </div>
                        <div className="input-field col s6">
                          {/* Time input */}
                          <input
                            placeholder="Time"
                            id={this.getTimeId(this.props.id)}
                            name="time"
                            type="text"
                            className="timepicker"
                          />
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                {/* Modal footer with schedule button */}
                <div class="modal-footer">
                  <a
                    href="#!"
                    class="modal-close btn-flat"
                    onClick={e => this.handleModalSubmit(this.props.id)}
                  >
                    Schedule
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RestaurantCard;
