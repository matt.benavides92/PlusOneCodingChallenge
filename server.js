const express = require('express');
const graphqlHTTP = require('express-graphql');
const cors = require('cors');
const schema = require('./schema');

const app = express();

// Allow cross-origin
const corsOptions = {
    origin: 'http://localhost:3000' // uncomment if using npm run dev or npm run server
    //origin: 'http://localhost:3001' // uncomment if using docker
};
app.use(cors(corsOptions));

app.use(
  '/graphql',
  graphqlHTTP({
    schema, //schema: schema,
    graphiql: true
  })
);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
